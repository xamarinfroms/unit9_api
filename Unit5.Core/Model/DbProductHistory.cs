﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unit5.Core
{
    [Table("ProductHistory")]
    public class DbProductHistory:Product
    {
        public DbProductHistory() { this.ViewTime = DateTime.Now; }
        public DbProductHistory(Product item)
        {
            this.ProductId = item.ProductId;
            this.Price = item.Price;
            this.Name = item.Name;
            this.IsOnSale = item.IsOnSale;
            this.ImageName = item.ImageName;
            this.Desc = item.Desc;
            this.Count = item.Count;
            this.ViewTime = DateTime.Now;
        }
        [PrimaryKey]
        public override Guid ProductId
        {
            get; set;
        }

        public DateTime ViewTime { get; set; }
    }
}
