﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unit5.Core
{
    public class ProductService
    {
        private SQLAccess sqlAccess;
        private RemoteAccess remoteAccess;
        public ProductService(string path)
        {
            sqlAccess = new SQLAccess(path);
            remoteAccess = new RemoteAccess();
        }

       public Task AddHistory(Product item)
        {
            return Task.Run(() =>
            sqlAccess.AddHistory(item));
        }

        public Task DeleteHistory(Guid id)
        {
            return Task.Run(() => sqlAccess.DeleteHistory(id));
        }

        public Task<IList<Product>> GetHistory()
        {
            return Task.Run<IList<Product>>(() => sqlAccess.GetProductHistory());
        }

        public Task<IList<Product>> GetProducts()
        {
            return remoteAccess.GetProducts();
        }
    }
}
