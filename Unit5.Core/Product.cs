﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unit5.Core
{
    public class Product
    {
        public override string ToString()
        {
            return this.Name;
        }
        public string Name { get; set; }


        public virtual Guid ProductId { get; set; }
        public  string ImageName { get; set; }
       
        public string Desc { get; set; }
        public int Price { get; set; }

        public int Count { get; set; } = 1;
        public bool IsOnSale { get; set; } 

    }
}
