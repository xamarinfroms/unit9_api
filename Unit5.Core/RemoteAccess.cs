﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Unit5.Core
{
    public class RemoteAccess
    {
        public async  Task<IList<Product>> GetProducts()
        {
            HttpClient requsest = new HttpClient();
            var json= await requsest.GetStringAsync("http://yottaclass.azurewebsites.net/Product/GetProducts");
             return  JsonConvert.DeserializeObject<IList<Product>>(json);
        }
    }
}
