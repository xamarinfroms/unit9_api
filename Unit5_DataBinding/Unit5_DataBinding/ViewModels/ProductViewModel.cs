﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unit5.Core;

namespace Unit5_DataBinding
{
    public class ProductViewModel: INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public ProductViewModel(Product data)
        {
            this.Product = data;
        }

        public string Name
        {
            get { return this.Product.Name; }
            set { this.Product.Name = value; this.FireUpdate(nameof(this.Name)); }
        }

        public string ImageName
        {
            get { return this.Product.ImageName; }
        }

        public string Desc
        {
            get { return this.Product.Desc; }
        }
        public int Price
        {
            get { return this.Product.Price; }
        }

        public int Count
        {
            get { return this.Product.Count; }

        }
        public bool IsOnSale
        {
            get { return this.Product.IsOnSale; }

        }

        public Guid ProductId
        {
            get { return this.Product.ProductId; }
        }

        public void FireUpdate(string name)
        {
            this.PropertyChanged?.DynamicInvoke(this, new PropertyChangedEventArgs(name));
        }



        public Product Product { get; set; }
    }
}
