﻿using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Unit5.Core;
using Xamarin.Forms;
using XamarinUniversity.Interfaces;

namespace Unit5_DataBinding
{
    public class ProductsViewModel : INotifyPropertyChanged
    {
        public INavigationService NavigationService { get; set; }
        public event EventHandler OnNoNtework;
        private ProductService service;


        public ProductsViewModel()
        {
            //ObservableCollection<ProductViewModel> items = new ObservableCollection<ProductViewModel>();
            //foreach (var item in ProductFactory.Products)
            //   items.Add(new ProductViewModel(item));



            //this.Products = items
            //    .OrderByDescending(c => c.Name)
            //    .GroupBy(c => c.Name[0].ToString())
            //    .Select(c => new ObservableGrouping<string, ProductViewModel>(c))
            //    .ToList();
            this.service = ((App)App.Current).ProductService;

            this.Products = new List<ObservableGrouping<string, ProductViewModel>>();

            this.OnUpdateData = new Command( () =>
              {
                //  this.Products[0].Name = "超級飲水機二代";
              });

            this.OnSortData = new Command(() =>
              {

                
              });

            this.OnGoDetail = new Command<Product>(this.GoDetail);


            this.OnRefresh = new Command(async () =>
              {
                  await Task.Delay(2000);;
                  this.IsRefreshing = false;
              });


        }


        public async void Init(bool connectionState)
        {

            if(!connectionState)
            {
                this.OnNoNtework(this, EventArgs.Empty);
                return;
            }
            var items =await service.GetProducts();
            ObservableCollection<ProductViewModel> datas = new ObservableCollection<ProductViewModel>();
            foreach (var item in items)
                datas.Add(new ProductViewModel(item));

            this.Products = datas
                .OrderByDescending(c => c.Name)
                .GroupBy(c => c.Name[0].ToString())
                .Select(c => new ObservableGrouping<string, ProductViewModel>(c))
                .ToList();
        }


        private async void GoDetail(Product item)
        {
            await this.NavigationService.NavigateAsync(AppPage.ProductDetail, new ProductDetailViewModel(item));
        }

        public List<ObservableGrouping<string, ProductViewModel>> Products
        {
            get { return this._products; }
            set { this._products = value;this.PropertyChanged?.DynamicInvoke(this, new PropertyChangedEventArgs(nameof(this.Products))); }
        }

        public Product SelectedProduct
        {
            get { return _selectedProduct; }
              set{ this._selectedProduct = value;this.PropertyChanged?.DynamicInvoke(this, new PropertyChangedEventArgs(nameof(this.SelectedProduct))); }
        }
        public string SearchText
        {
            get {   return this._searchText;  }
            set {   _searchText = value; this.PropertyChanged?.DynamicInvoke(this, new PropertyChangedEventArgs(nameof(this.SearchText))); }
        }
   


        public HeaderViewModel Header { get; set; } = new HeaderViewModel();

        public bool IsRefreshing
        {
            get { return this._isRefreshing; }
            set
            {
                this._isRefreshing = value;
                this.PropertyChanged?.DynamicInvoke(this, new PropertyChangedEventArgs(nameof(this.IsRefreshing)));
            }
        }

        public ICommand OnQuery { get; set; }
        public ICommand OnRefresh { get; private set; }
        public ICommand OnSortData { get; set; }
        public ICommand OnUpdateData { get; private set; }
        public ICommand OnGoDetail { get; private set; }



        private string _searchText;
        private Product _selectedProduct;
        private List<ObservableGrouping<string, ProductViewModel>> _products;
        private bool _isRefreshing;
        public event PropertyChangedEventHandler PropertyChanged;
    }

    public static class ObservableExt
    {
        public static List<ObservableGrouping<TKey, TElement>> ToObservableGrouping<TKey, TElement>(this IEnumerable<IGrouping<TKey, TElement>> groupData)
        {
           return groupData.Select(c => new ObservableGrouping<TKey, TElement>(c)).ToList();
        }
    }

    public class ObservableGrouping<TKey, TElement> : ObservableCollection<TElement>
    {
        public TKey Key { get; set; }
        public ObservableGrouping(IGrouping<TKey,TElement> groupData)
        {
            this.Key = groupData.Key;
            foreach (var item in groupData)
                this.Add(item);
        }
    }

    public class HeaderViewModel
    {
        public string Text { get; set; } = "貓砂工廠廣告";
        public string Image { get; set; } = "header.png";
    }
}
