﻿using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Unit5.Core;
using Xamarin.Forms;
using XamarinUniversity.Interfaces;
using XamarinUniversity.Services;

namespace Unit5_DataBinding
{
    public partial class App : Application
    {
        public  static string SqlPath { get; private set; }
        public ProductService ProductService { get; set; }
        public App()
        {
            //FormsNavigationPageService service = new FormsNavigationPageService();
            //service.RegisterPage(AppPage.Info, () => new Info());
            //service.RegisterPage(AppPage.Login, () => new Login(service));
            //service.RegisterPage(AppPage.Product, () => new Products(service));
            //service.RegisterPage(AppPage.ProductDetail, () => new ProductDetail());



            InitializeComponent();
            this.MainPage = new Login(null);
            this.ProductService = new ProductService(DependencyService.Get<ISQLPath>().GetSQLiteFolder());


            this.ModalPopped += App_ModalPopped;
            this.ModalPopping += App_ModalPopping;
            this.ModalPushed += App_ModalPushed;
            this.ModalPushing += App_ModalPushing;

            CrossConnectivity.Current.ConnectivityChanged += (sender, e) =>
            {
                
            };
        }



        private void App_ModalPushing(object sender, ModalPushingEventArgs e)
        {
            
            throw new NotImplementedException();
        }

        private void App_ModalPushed(object sender, ModalPushedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void App_ModalPopping(object sender, ModalPoppingEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void App_ModalPopped(object sender, ModalPoppedEventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}
