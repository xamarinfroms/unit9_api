﻿using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unit5.Core;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinUniversity.Interfaces;

namespace Unit5_DataBinding
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Products : ContentPage
    {
        public Products(INavigationService nav)
        {
            var type = CrossConnectivity.Current.ConnectionTypes.ToList();



            InitializeComponent();
            var viewModel = new ProductsViewModel() { NavigationService = nav };
            viewModel.OnNoNtework += delegate { this.DisplayAlert("提示", "無網路連線", "確定"); };
            viewModel.Init(CrossConnectivity.Current.IsConnected);
            this.BindingContext = viewModel;
            this.Title = "商品列表";

           

            BindingBase.EnableCollectionSynchronization(
                viewModel.Products, 
                null,
                (datas, context, action, writeAccess) =>
                {
                    lock (datas)
                    {
                        action();
                    }
                });
  
        }

        private async void list_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            this.list.SelectedItem = null;
            var data = e.Item as ProductViewModel;
            if (data.Count == 0)
            {
                await this.DisplayAlert("提示", "商品已售完", "OK");
                return;
            }
            
            var viewModel = this.BindingContext as ProductsViewModel;
            ProductDetailCarousel detailPage = new ProductDetailCarousel(viewModel.Products.SelectMany(c => c.ToList()).ToList());
            detailPage.SelectedItem = e.Item;
            var app=App.Current as App;
           await  app.ProductService.AddHistory(((ProductViewModel)e.Item).Product);
           await this.Navigation.PushAsync(detailPage);
        }

        private void list_Refreshing(object sender, EventArgs e)
        {
            //更新資料...
            //...
          //  this.list.IsRefreshing = false;
        }
    }

    [ContentProperty("Source")]
    public class ImageMak : IMarkupExtension
    {
        public string Source { get; set; }
        public object ProvideValue(IServiceProvider serviceProvider)
        {
            return ImageSource.FromResource($"Unit5_DataBinding.Images.{Source}");
        }
    }
    public class ProductTemplateSelector : DataTemplateSelector
    {
        public DataTemplate Normal { get; set; }
        public DataTemplate Soldout { get; set; }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            var data = item as ProductViewModel;
            return data.Count == 0 ? Soldout : Normal;
        }
    }
}
