﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinUniversity.Interfaces;

namespace Unit5_DataBinding
{
    
    public partial class Login : ContentPage
    {

        public Login(INavigationService nav)
        {
            this.BindingContext = new LoginViewModel() { Navigation = nav };
            InitializeComponent();

            swRember.IsToggled = Settings.isRemember;
            txtAccount.Text = Settings.Account;


        }

        private  void Button_Clicked(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtAccount.Text))
            {
                this.DisplayAlert("提示", "帳號未輸入", "確定");
                return;
            }
            if (string.IsNullOrEmpty(txtPwd.Text))
            {
                this.DisplayAlert("提示", "密碼未輸入", "確定");
                return;
            }

            Settings.Account = this.txtAccount.Text;
            Settings.isRemember = this.swRember.IsToggled;

            App.Current.MainPage = new MasterDetailPage()
            {
                Master = new Menu(),
                Detail = new NavigationPage(new Products(null))
            };
        }
    }
}
