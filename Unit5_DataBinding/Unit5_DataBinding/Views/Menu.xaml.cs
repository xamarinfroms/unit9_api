﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Unit5_DataBinding
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Menu : ContentPage
    {
        public Menu()
        {
            InitializeComponent();
            this.menuList.ItemsSource = new string[]
            {
                "商品列表","我的資訊","登出"
            };

            this.menuList.ItemTapped += MenuList_ItemTapped;
        }

        private void MenuList_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var md = this.Parent as MasterDetailPage;
            switch (e.Item.ToString())
            {
                case "商品列表":
                    md.Detail = new NavigationPage(new Products(null));
                    break;
               
                case "我的資訊":
                    TabbedPage tabs = new TabbedPage();
                    Info info = new Info();
                    History history = new History();
                    tabs.Children.Add(info);
                    tabs.Children.Add(history);

                    md.Detail = new NavigationPage(tabs);
                    tabs.Title = info.Title;
                    tabs.CurrentPageChanged += delegate
                    {
                        tabs.Title = tabs.CurrentPage.Title;
                    };
                    break;

                case "登出":
                    App.Current.MainPage = new Login(null);
                    break;
            }

       
            md.IsPresented = false;
            this.menuList.SelectedItem = null;
        }
    }
}
