﻿


using System;
using Unit5_DataBinding.Droid;

[assembly: Xamarin.Forms.Dependency(typeof(SQLPath))]
namespace Unit5_DataBinding.Droid
{
    public class SQLPath : ISQLPath
    {
        public string GetSQLiteFolder()
        {
            return Environment.GetFolderPath(Environment.SpecialFolder.Personal);
        }
    }
}