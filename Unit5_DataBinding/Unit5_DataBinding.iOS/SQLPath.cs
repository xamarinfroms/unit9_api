﻿


using System;
using System.IO;
using Unit5_DataBinding.iOS;

[assembly:Xamarin.Forms.Dependency(typeof(SQLPath))]
namespace Unit5_DataBinding.iOS
{
    public class SQLPath : ISQLPath
    {
        public string GetSQLiteFolder()
        {
            var folder= Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            var libFolder = Path.Combine(folder, "..", "Library");
            return libFolder;
        }
    }
}